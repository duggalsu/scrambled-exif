# Scrambled Exif

Some people like Facebook and co. to know where their picture was taken so that it can be easily geotagged. Or for whatever reason. That's okay.

Some people *don't* like that. That's also okay.

**Scrambled Exif** (pronounced *egg* sif) is definitely for the latter group of people.

So you don't want to share all the metadata your phone stores for every picture?

This Android app will remove all this data before sharing. Just share a picture like you'd normally do and choose *Scrambled Exif*. A moment later, the share 'dialog' will reappear. Just share with the app you intended to share with in the first place. Et voilà!

## Required Android Permissions:

- READ_EXTERNAL_STORAGE to read the images other apps share with it.

## Download it

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/com.jarsilio.android.scrambledeggsif)

[<img src="https://gitlab.com/juanitobananas/wave-up/raw/master/google-play-store/google-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif)

## Translations

If you wish to help to translate this app to your own language, you can do so by translating these two projects using transifex:
[scrambled-exif](https://www.transifex.com/juanitobananas/scrambled-exif/ "Scrambled Exif on transifex") and
[libcommon](https://www.transifex.com/juanitobananas/libcommon/ "libcommon on transifex").

## Misc Stuff and Facts

- Basically, Exif is used by jpeg, which is the format in which your Android camera saves pictures. If you want to know more about Exif, check the [Wikipedia](https://en.wikipedia.org/wiki/Exif).

- Scrambled Exif also renames the files.

- Please don't heavily rely on the data being deleted. Scrambled Exif does its job pretty well, but it could fail. Always double-check before you share.

- It doesn't really *scramble* the Exif data, it deletes it. So the name is probably stupid. But I like it. The icon doesn't depict scrambled eggs either. So the icon is probably stupid. But I like it. And I also happen to be a huge fan of eggs. So this app doesn't only have a stupid name and an equally stupid icon, it is also my (non-stupid) tribute to eggs. Especially *huevos fritos*. Because I love them.

- Have fun sharing your pics!
 
## Legal Attributions

Google Play and the Google Play logo are trademarks of Google Inc.
